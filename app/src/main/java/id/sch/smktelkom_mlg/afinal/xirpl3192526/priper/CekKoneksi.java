package id.sch.smktelkom_mlg.afinal.xirpl3192526.priper;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class CekKoneksi {

    private static final String TAG = CekKoneksi.class.getSimpleName();

    public static boolean SatusInternet(Context context) {
        NetworkInfo info = ((ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();

        if (info == null) {
            Log.d(TAG, "Tidak Ada Koneksi Internet");
            return false;
        } else {
            if (info.isConnected()) {
                Log.d(TAG, " Koneksi Internet Tersedia...");
                return true;
            } else {
                Log.d(TAG, " Koneksi Internet Tidak Tersedia");
                return true;
            }

        }
    }
}