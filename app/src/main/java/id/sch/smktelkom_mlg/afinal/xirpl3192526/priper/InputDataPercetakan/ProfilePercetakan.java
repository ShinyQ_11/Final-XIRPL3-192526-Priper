package id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.InputDataPercetakan;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.shashank.sony.fancytoastlib.FancyToast;

import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.CekKoneksi;
import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.R;

/**
 * Created by SMK TELKOM on 4/30/2018.
 */

public class ProfilePercetakan extends AppCompatActivity implements View.OnClickListener {
    private FirebaseAuth auth;
    private FirebaseUser user;
    private FirebaseAuth.AuthStateListener authStateListener;
    private DatabaseReference reference;
    private android.support.v7.widget.Toolbar toolbar;

    private Button Edit, SignOut;
    private TextView Nama, Email, Telp, Username, Id, Alamat;
    private ImageView Foto;

    private ProgressDialog progressDialog;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_percetakan);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Tunggu Sebentar ...");
        progressDialog.show();
        auth = FirebaseAuth.getInstance();
        user = FirebaseAuth.getInstance().getCurrentUser();
        final String Uid = user.getUid();
        reference = FirebaseDatabase.getInstance().getReference().child("Percetakan").child(Uid).child("Data Percetakan");

        if (CekKoneksi.SatusInternet(ProfilePercetakan.this)) //returns true if internet available
        {
            reference = FirebaseDatabase.getInstance().getReference().child("Data Percetakan").child(Uid);
            reference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    String name = dataSnapshot.child("nama").getValue(String.class);
                    String telp = dataSnapshot.child("telp").getValue(String.class);
                    String email = dataSnapshot.child("email").getValue(String.class);
                    String username = dataSnapshot.child("username").getValue(String.class);
                    String foto = dataSnapshot.child("foto").getValue(String.class);
                    String alamat = dataSnapshot.child("alamat").getValue(String.class);

                    Id.setText("@" + Uid);
                    Nama.setText(name);
                    Email.setText(email);
                    Telp.setText(telp);
                    Username.setText(username);
                    Alamat.setText(alamat);
                    if (foto == null) {
                        Foto.setImageResource(R.drawable.profile);
                    } else {
                        Glide.with(ProfilePercetakan.this)
                                .load(foto)
                                .into(Foto);
                    }

                    progressDialog.dismiss();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    finish();
                }
            });

        } else {
            FancyToast.makeText(ProfilePercetakan.this, " Koneksi Internet Tidak Ada ! ", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
        }

        toolbar = findViewById(R.id.tb_main);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Id = findViewById(R.id.tv_id);
        Nama = findViewById(R.id.tv_nama);
        Email = findViewById(R.id.tv_email);
        Telp = findViewById(R.id.tv_telp);
        Alamat = findViewById(R.id.tv_alamat);
        Username = findViewById(R.id.tv_username);
        Foto = findViewById(R.id.profile_image);

        Edit = findViewById(R.id.btn_edit);
        SignOut = findViewById(R.id.btn_signout);
        Edit.setOnClickListener(this);
        SignOut.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_signout:
                auth.signOut();
                finish();
                break;

            case R.id.btn_edit:
                Intent intentEditPercetakan = new Intent(ProfilePercetakan.this, EditProfilePercetakan.class);
                startActivity(intentEditPercetakan);
                break;
        }
    }
}
