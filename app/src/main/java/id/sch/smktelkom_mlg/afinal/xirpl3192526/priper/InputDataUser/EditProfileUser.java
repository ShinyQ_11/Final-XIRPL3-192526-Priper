package id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.InputDataUser;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.shashank.sony.fancytoastlib.FancyToast;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.R;


public class EditProfileUser extends AppCompatActivity implements View.OnClickListener {

    private static final int SELECT_FILE = 2;
    private android.support.v7.widget.Toolbar toolbar;
    private TextView Nama, Telp, Username, Alamat;
    private Button Edit;

    //FIREBASE DATABASE DAN STORAGE
    private DatabaseReference mUserDatabse;
    private StorageReference mStorageRef;

    //FIREBASE AUTH
    private FirebaseAuth mAuth;
    private FirebaseUser user;

    private DatabaseReference reference;

    private ImageView FotoUser;
    private Button btnGallery;
    private Uri imageHoldUri = null;

    private ProgressDialog progressDialog;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editprofil);

        btnGallery = findViewById(R.id.btn_edit_foto);
        FotoUser = findViewById(R.id.profile_image);

        user = FirebaseAuth.getInstance().getCurrentUser();
        mAuth = FirebaseAuth.getInstance();

        toolbar = findViewById(R.id.tb_main);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        progressDialog = new ProgressDialog(this);
        Alamat = findViewById(R.id.edt_alamat);
        Nama = findViewById(R.id.edt_nama);
        Telp = findViewById(R.id.edt_telp);
        Username = findViewById(R.id.edt_username);

        Edit = findViewById(R.id.btn_edit);
        Edit.setOnClickListener(this);

        mUserDatabse = FirebaseDatabase.getInstance().getReference().child("Users").child(mAuth.getCurrentUser().getUid());
        getUserProfil();
        mStorageRef = FirebaseStorage.getInstance().getReference();


        //USER IMAGEVIEW ONCLICK LISTENER
        btnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profilePicSelection();

            }
        });

    }

    private void profilePicSelection() {


        //MEMILIH GAMBAR PADA GALLERY
        final CharSequence[] items = {"Pilih Dari Galeri",
                "Batal"};
        AlertDialog.Builder builder = new AlertDialog.Builder(EditProfileUser.this);
        builder.setTitle("Tambahkan Foto");

        //SET ITEMS LISTENERS
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Pilih Dari Galeri")) {
                    galleryIntent();
                } else if (items[item].equals("Batal")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();

    }

    private void galleryIntent() {

        //Pilih IMAGE DARI GALLERY
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, SELECT_FILE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        //SAVE URI FROM GALLERY
        if (requestCode == SELECT_FILE && resultCode == RESULT_OK) {
            Uri imageUri = data.getData();

            CropImage.activity(imageUri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setAspectRatio(1, 1)
                    .start(this);

        }

        //image crop library code
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                imageHoldUri = result.getUri();

                FotoUser.setImageURI(imageHoldUri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {

            }
        }

    }


    private void getUserProfil() {
        final String Uid = user.getUid();
        reference = FirebaseDatabase.getInstance().getReference().child("Costumer").child(Uid).child("Data User");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String name = dataSnapshot.child("nama").getValue(String.class);
                String telp = dataSnapshot.child("telp").getValue(String.class);
                String username = dataSnapshot.child("username").getValue(String.class);
                String alamat = dataSnapshot.child("alamat").getValue(String.class);
                String foto = dataSnapshot.child("foto").getValue(String.class);

                Alamat.setText(alamat);
                Nama.setText(name);
                Telp.setText(telp);
                Username.setText(username);
                if (foto == null) {
                    FotoUser.setImageResource(R.drawable.profile);
                } else {
                    Glide.with(EditProfileUser.this)
                            .load(foto)
                            .into(FotoUser);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btn_edit:
                if (Nama.getText().toString().length() == 0) {
                    FancyToast.makeText(EditProfileUser.this, " Nama Harus Diisi ! ", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
                } else if (Telp.getText().toString().length() == 0) {
                    FancyToast.makeText(EditProfileUser.this, " Nomor Telepon Harus Diisi ! ", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
                } else if (Username.getText().toString().length() == 0) {
                    FancyToast.makeText(EditProfileUser.this, " Username Harus Diisi ! ", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
                } else {
                    final String name = Nama.getText().toString().trim();
                    final String telp = Telp.getText().toString().trim();
                    final String username = Username.getText().toString().trim();
                    final String alamat = Alamat.getText().toString().trim();
                    progressDialog.setMessage("Silahkan Tunggu...");
                    progressDialog.show();
                    // Get UID User Untuk Update Data Pada Realtime Database
                    final String Uid = mAuth.getCurrentUser().getUid();
                    final DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Costumer").child(Uid).child("Data User");
                    reference.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (imageHoldUri == null) {
                                reference.child("alamat").setValue(alamat);
                                reference.child("nama").setValue(name);
                                reference.child("telp").setValue(telp);
                                reference.child("username").setValue(username);
                                FancyToast.makeText(EditProfileUser.this, "Edit Profil Sukses", FancyToast.LENGTH_LONG, FancyToast.SUCCESS, false).show();
                                progressDialog.dismiss();
                                finish();


                            } else {
                                StorageReference mChildStorage = mStorageRef.child("Costumer/" + Uid).child("FotoProfil.jpg");
                                mChildStorage.putFile(imageHoldUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                    @Override
                                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                        final Uri imageUrl = taskSnapshot.getDownloadUrl();
                                        String profilePicUrl = imageUrl.toString().trim();
                                        reference.child("nama").setValue(name);
                                        reference.child("telp").setValue(telp);
                                        reference.child("username").setValue(username);
                                        reference.child("alamat").setValue(alamat);
                                        reference.child("foto").setValue(profilePicUrl);
                                        FancyToast.makeText(EditProfileUser.this, "Edit Profil Sukses", FancyToast.LENGTH_LONG, FancyToast.SUCCESS, false).show();
                                        progressDialog.dismiss();

                                    }
                                });
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            FancyToast.makeText(EditProfileUser.this, "Edit Profil Sukses", FancyToast.LENGTH_LONG, FancyToast.SUCCESS, false).show();
                            progressDialog.dismiss();
                            finish();
                        }
                    });

                    break;
                }
        }

    }
}




