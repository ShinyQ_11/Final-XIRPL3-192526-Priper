package id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.InputDataUser;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.shashank.sony.fancytoastlib.FancyToast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.R;
import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.RecyclerTempatPercetakan.PercetakanActivity;
import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.StatusPrint.OrderPending;

public class PrintFile extends AppCompatActivity implements View.OnClickListener {

    private final String STATUS = "Pending";
    private Uri fileUri;
    private android.support.v7.widget.Toolbar toolbar;
    private EditText Jumlah, Note;
    private Button Pesan, Tempat;
    private Spinner SpinnerJenisKertas, SpinnerTinta, SpinnerOrientasi;
    private ArrayAdapter<CharSequence> adapterJenisKertas, adapterTinta, adapterOrientasi;
    private Calendar calendar;
    private SimpleDateFormat simpleDateFormat;
    private FirebaseUser user;
    private TextView notification, tempatCetak;
    private DatabaseReference reference, ref;
    private FirebaseAuth mAuth;
    private StorageReference storage;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_print);
        progressDialog = new ProgressDialog(this);

        reference = FirebaseDatabase.getInstance().getReference().child("Costumer");
        ref = FirebaseDatabase.getInstance().getReference().child("Percetakan");
        user = FirebaseAuth.getInstance().getCurrentUser();
        mAuth = FirebaseAuth.getInstance();
        storage = FirebaseStorage.getInstance().getReference();

        toolbar = findViewById(R.id.tb_main);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tempatCetak = findViewById(R.id.tv_tempat);
        Jumlah = findViewById(R.id.edt_jumlah);
        Note = findViewById(R.id.edt_note);
        Pesan = findViewById(R.id.btn_pesan);
        Pesan.setOnClickListener(this);
        Button btn_pilih_file = findViewById(R.id.btn_pilih_file);
        notification = findViewById(R.id.notification);
        Tempat = findViewById(R.id.btn_pilih_tempat);
        Tempat.setOnClickListener(this);

        SpinnerJenisKertas = findViewById(R.id.spinnercustom);
        adapterJenisKertas = ArrayAdapter.createFromResource(this, R.array.Jenis_Kertas, android.R.layout.simple_spinner_item);
        adapterJenisKertas.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SpinnerJenisKertas.setAdapter(adapterJenisKertas);

        SpinnerTinta = findViewById(R.id.spinnerWarna);
        adapterTinta = ArrayAdapter.createFromResource(this, R.array.Jenis_Tinta, android.R.layout.simple_spinner_item);
        adapterTinta.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SpinnerTinta.setAdapter(adapterTinta);

        SpinnerOrientasi = findViewById(R.id.spinnerOrientasi);
        adapterOrientasi = ArrayAdapter.createFromResource(this, R.array.Orientasi, android.R.layout.simple_spinner_item);
        adapterOrientasi.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SpinnerOrientasi.setAdapter(adapterOrientasi);

        final String namaPercetakan = getIntent().getStringExtra("NamaPercetakan");
        if (namaPercetakan == null) {
            tempatCetak.setText("Anda Belum Memilih");
        } else {
            tempatCetak.setText(namaPercetakan);
        }

        btn_pilih_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(PrintFile.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    selectFile();
                } else {
                    ActivityCompat.requestPermissions(PrintFile.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 9);
                }
            }
        });

    }

    private void selectFile() {
        Intent intent = new Intent();
        intent.setType("application/pdf");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, 86);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 9 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            selectFile();
        } else
            FancyToast.makeText(PrintFile.this, "Tolong Berikan Permission", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 86 && resultCode == RESULT_OK && data != null) {
            fileUri = data.getData();
            notification.setText("File Telah Dipilih");
        } else {
            Toast.makeText(PrintFile.this, "Silahkan Pilih File", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btn_pesan:
                pesanan();
                break;

            case R.id.btn_pilih_tempat:
                Intent intentTempat = new Intent(PrintFile.this, PercetakanActivity.class);
                finish();
                startActivity(intentTempat);
        }
    }

    @SuppressLint("SimpleDateFormat")
    public void pesanan() {
        if (Jumlah.getText().toString().length() == 0) {
            FancyToast.makeText(PrintFile.this, " Jumlah Yang Di Print Harus Diisi ! ", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
        } else if (fileUri == null) {
            FancyToast.makeText(PrintFile.this, " Anda Belum Memasukkan File ! ", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
        } else if (tempatCetak.equals("Anda Belum Memilih")) {
            FancyToast.makeText(PrintFile.this, " Anda Belum Memilih Percetakan ! ", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
        } else {
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setTitle("Uploading File...");
            progressDialog.setProgress(0);
            progressDialog.show();

            final String Uid = user.getUid();
            final StorageReference storageReference = storage;

            final String fileName = System.currentTimeMillis() + "";
            final String jumlah = Jumlah.getText().toString().trim();
            final String note = Note.getText().toString().trim();
            final String jenisKertas = SpinnerJenisKertas.getSelectedItem().toString().trim();
            final String jenisOrientasi = SpinnerOrientasi.getSelectedItem().toString().trim();
            final String jenisTinta = SpinnerTinta.getSelectedItem().toString().trim();

            calendar = Calendar.getInstance();
            final String tanggal = DateFormat.getDateInstance(DateFormat.FULL).format(calendar.getTime());

            calendar = Calendar.getInstance();
            simpleDateFormat = new SimpleDateFormat("HH:mm");
            final String Jam = simpleDateFormat.format(calendar.getTime());

            FirebaseDatabase database = FirebaseDatabase.getInstance();

            // Membuat ID Untuk Transaksi
            final String key = database.getReference("Transaksi").push().getKey();

            // Get Data Tempat Print
            final String id_percetakan = getIntent().getStringExtra("id_percetakan");
            final String namaPercetakan = getIntent().getStringExtra("NamaPercetakan");

            storageReference.child("Costumer").child(Uid).child("Transaksi").child(fileName).putFile(fileUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            final String url = taskSnapshot.getDownloadUrl().toString();

                            reference = FirebaseDatabase.getInstance().getReference().child("Costumer").child(Uid).child("Data User");
                            reference.addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    String name = dataSnapshot.child("nama").getValue(String.class);

                                    reference = FirebaseDatabase.getInstance().getReference().child("Costumer");
                                    DatabaseReference TransaksidbUser = reference.child(Uid).child("Transaksi").child(key);
                                    TransaksidbUser.child("id_transaksi").setValue(key);
                                    TransaksidbUser.child("id_user").setValue(Uid);
                                    TransaksidbUser.child("jumlah").setValue(jumlah);
                                    TransaksidbUser.child("catatan").setValue(note);
                                    TransaksidbUser.child("jenis_kertas").setValue(jenisKertas);
                                    TransaksidbUser.child("jenis_orientasi").setValue(jenisOrientasi);
                                    TransaksidbUser.child("jenis_tinta").setValue(jenisTinta);
                                    TransaksidbUser.child("jam").setValue(Jam);
                                    TransaksidbUser.child("tanggal").setValue(tanggal);
                                    TransaksidbUser.child("id_percetakan").setValue(id_percetakan);
                                    TransaksidbUser.child("nama_percetakan").setValue(namaPercetakan);
                                    TransaksidbUser.child("status").setValue(STATUS);
                                    TransaksidbUser.child("nama").setValue(name);
                                    TransaksidbUser.child("file").setValue(url);

                                    ref = FirebaseDatabase.getInstance().getReference().child("Percetakan");
                                    DatabaseReference TransaksidbPercetakan = ref.child(id_percetakan).child("Transaksi").child(key);
                                    TransaksidbPercetakan.child("id_transaksi").setValue(key);
                                    TransaksidbPercetakan.child("id_user").setValue(Uid);
                                    TransaksidbPercetakan.child("jumlah").setValue(jumlah);
                                    TransaksidbPercetakan.child("catatan").setValue(note);
                                    TransaksidbPercetakan.child("jenis_kertas").setValue(jenisKertas);
                                    TransaksidbPercetakan.child("jenis_orientasi").setValue(jenisOrientasi);
                                    TransaksidbPercetakan.child("jenis_tinta").setValue(jenisTinta);
                                    TransaksidbPercetakan.child("jam").setValue(Jam);
                                    TransaksidbPercetakan.child("tanggal").setValue(tanggal);
                                    TransaksidbPercetakan.child("id_percetakan").setValue(id_percetakan);
                                    TransaksidbPercetakan.child("nama_percetakan").setValue(namaPercetakan);
                                    TransaksidbPercetakan.child("nama").setValue(name);
                                    TransaksidbPercetakan.child("status").setValue(STATUS);
                                    TransaksidbPercetakan.child("file").setValue(url).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                Handler handler = new Handler();
                                                handler.postDelayed(new Runnable() {
                                                    public void run() {
                                                        progressDialog.dismiss();
                                                        Intent IntentPrint = new Intent(PrintFile.this, OrderPending.class);
                                                        IntentPrint.putExtra("KeyTransaksi", key);
                                                        IntentPrint.putExtra("NamaPercetakan", namaPercetakan);
                                                        startActivity(IntentPrint);
                                                        finish();
                                                    }
                                                }, 300);
                                            } else {

                                            }

                                        }
                                    });

                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });


                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(PrintFile.this, "File Gagal Upload", Toast.LENGTH_SHORT).show();

                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    int currentProgress = (int) (100 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                    progressDialog.setProgress(currentProgress);
                }
            });
        }

    }
}


