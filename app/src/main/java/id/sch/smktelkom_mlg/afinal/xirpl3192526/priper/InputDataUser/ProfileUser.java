package id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.InputDataUser;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.shashank.sony.fancytoastlib.FancyToast;

import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.CekKoneksi;
import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.R;

public class ProfileUser extends AppCompatActivity implements View.OnClickListener {

    FirebaseAuth auth;
    FirebaseUser user;
    DatabaseReference reference;
    android.support.v7.widget.Toolbar toolbar;

    Button Edit, SignOut;
    TextView Nama, Email, Telp, Username, Id, Alamat;
    ImageView Foto;

    private ProgressDialog progressDialog;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Tunggu Sebentar ...");
        progressDialog.show();

        user = FirebaseAuth.getInstance().getCurrentUser();
        final String Uid = user.getUid();
        reference = FirebaseDatabase.getInstance().getReference().child("Costumer").child(Uid).child("Data User");
        auth = FirebaseAuth.getInstance();

        if (CekKoneksi.SatusInternet(ProfileUser.this)) //returns true if internet available
        {
            reference = FirebaseDatabase.getInstance().getReference().child("Costumer").child(Uid).child("Data User");
            reference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    String alamat = dataSnapshot.child("alamat").getValue(String.class);
                    String name = dataSnapshot.child("nama").getValue(String.class);
                    String telp = dataSnapshot.child("telp").getValue(String.class);
                    String email = dataSnapshot.child("email").getValue(String.class);
                    String username = dataSnapshot.child("username").getValue(String.class);
                    String foto = dataSnapshot.child("foto").getValue(String.class);

                    Id.setText("@" + Uid);
                    Alamat.setText(alamat);
                    Nama.setText(name);
                    Email.setText(email);
                    Telp.setText(telp);
                    Username.setText(username);
                    if (foto == null || foto == "") {
                        Foto.setImageResource(R.drawable.profile);
                    } else {
                        Glide.with(ProfileUser.this)
                                .load(foto)
                                .into(Foto);
                    }

                    progressDialog.dismiss();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        } else {
            FancyToast.makeText(ProfileUser.this, " Koneksi Internet Tidak Ada ! ", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
        }

        toolbar = findViewById(R.id.tb_main);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Id = findViewById(R.id.tv_id);
        Nama = findViewById(R.id.tv_nama);
        Email = findViewById(R.id.tv_email);
        Telp = findViewById(R.id.tv_telp);
        Username = findViewById(R.id.tv_username);
        Foto = findViewById(R.id.profile_image);
        Alamat = findViewById(R.id.tv_alamat);

        Edit = findViewById(R.id.btn_edit);
        SignOut = findViewById(R.id.btn_signout);
        Edit.setOnClickListener(this);
        SignOut.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_signout:
                finish();
                auth.signOut();
                break;

            case R.id.btn_edit:
                Intent intentEdit = new Intent(ProfileUser.this, EditProfileUser.class);
                startActivity(intentEdit);
                break;
        }
    }


}
