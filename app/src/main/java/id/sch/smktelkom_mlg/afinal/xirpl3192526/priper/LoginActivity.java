package id.sch.smktelkom_mlg.afinal.xirpl3192526.priper;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.shashank.sony.fancytoastlib.FancyToast;

import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.MainActivity.MainActivity;
import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.MainActivity.MainActivityPercetakan;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText Email, Password;
    private Button Login;
    private TextView Daftar;
    private ProgressDialog progressDialog;
    private DatabaseReference reference;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        progressDialog = new ProgressDialog(this);

        Email = findViewById(R.id.edt_email);
        Password = findViewById(R.id.edt_password);

        Daftar = findViewById(R.id.tv_Daftar);
        Daftar.setOnClickListener(this);

        Login = findViewById(R.id.btn_Login);
        Login.setOnClickListener(this);

        mAuth = FirebaseAuth.getInstance();
        reference = FirebaseDatabase.getInstance().getReference().child("Costumer");

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_Login:
                if (Email.getText().toString().length() == 0) {
                    FancyToast.makeText(LoginActivity.this, " Email Harus Diisi ! ", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
                } else if (Password.getText().toString().length() == 0) {
                    FancyToast.makeText(LoginActivity.this, " Password Harus Diisi ! ", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
                } else {
                    progressDialog.setMessage("Tunggu Sebentar ...");
                    progressDialog.show();

                    String email = Email.getText().toString().trim();
                    String password = Password.getText().toString().trim();

                    mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                progressDialog.dismiss();

                                checkUser();
                                FancyToast.makeText(LoginActivity.this, "Login Berhasil", FancyToast.LENGTH_LONG, FancyToast.SUCCESS, false).show();
                            } else {
                                progressDialog.dismiss();
                                FancyToast.makeText(LoginActivity.this, "Email Atau Password Salah !", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
                            }
                        }
                    });
                }
                break;

            case R.id.tv_Daftar:
                Intent IntentDaftar = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(IntentDaftar);
                finish();
                break;
        }

    }


    private void checkUser() {
        final String user_uid = mAuth.getCurrentUser().getUid();
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild(user_uid)) {

                    final String Uid = mAuth.getCurrentUser().getUid();
                    reference = FirebaseDatabase.getInstance().getReference().child("Costumer").child(Uid).child("Data User");
                    reference.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            String statususer = dataSnapshot.child("jenis_pengguna").getValue(String.class);
                            if (statususer.equals("Pengguna") || statususer == "pengguna") {
                                Intent intentLogin = new Intent(LoginActivity.this, MainActivity.class);
                                startActivity(intentLogin);
                                intentLogin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                finish();

                            } else {
                                Intent intentLogin = new Intent(LoginActivity.this, MainActivityPercetakan.class);
                                startActivity(intentLogin);
                                intentLogin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                finish();
                            }

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            finish();
                        }
                    });
                } else {
                    progressDialog.dismiss();
                    FancyToast.makeText(LoginActivity.this, " Akun Tidak Ditemukan ! ", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
                    finish();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                finish();
            }
        });
    }

}
