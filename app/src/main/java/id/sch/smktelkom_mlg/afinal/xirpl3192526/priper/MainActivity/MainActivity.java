package id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.MainActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;

import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.About.TentangKami;
import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.InputDataUser.PrintFile;
import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.InputDataUser.ProfileUser;
import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.LoginActivity;
import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.R;
import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.RecyclerPesananUser.StatusPesananActivity;
import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.RecyclerRiwayatPercetakan.RiwayatUserRecycler;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private android.support.v7.widget.Toolbar toolbar;
    private DatabaseReference reference;
    private FirebaseAuth auth;
    private FirebaseAuth.AuthStateListener authStateListener;
    private ImageButton Profil, History, Status, Print;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Profil = findViewById(R.id.btn_profil);
        History = findViewById(R.id.btn_history);
        Status = findViewById(R.id.btn_status);
        Print = findViewById(R.id.btn_print);

        Profil.setOnClickListener(this);
        History.setOnClickListener(this);
        Status.setOnClickListener(this);
        Print.setOnClickListener(this);

        auth = FirebaseAuth.getInstance();

        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() == null) {

                    Intent intentLogin = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(intentLogin);
                    finish();
                }
            }
        };

        toolbar = findViewById(R.id.tb_main);
        setSupportActionBar(toolbar);
        toolbar.inflateMenu(R.menu.main_menu);
    }

    @Override
    public void onStart() {
        super.onStart();
        // Supaya Validasi User Sudah Login Atau Belum Berjalan
        auth.addAuthStateListener(authStateListener);
    }

    public void onStop() {
        super.onStop();
        if (authStateListener != null) {
            auth.removeAuthStateListener(authStateListener);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.about:
                startActivity(new Intent(MainActivity.this, TentangKami.class));
                break;

        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_profil:
                Intent ProfilIntent = new Intent(MainActivity.this, ProfileUser.class);
                startActivity(ProfilIntent);
                ProfilIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                break;

            case R.id.btn_history:
                Intent intentHistory = new Intent(MainActivity.this, RiwayatUserRecycler.class);
                startActivity(intentHistory);
                intentHistory.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                break;

            case R.id.btn_status:
                Intent intentStatus = new Intent(MainActivity.this, StatusPesananActivity.class);
                startActivity(intentStatus);
                intentStatus.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                break;

            case R.id.btn_print:
                Intent intentPrint = new Intent(MainActivity.this, PrintFile.class);
                startActivity(intentPrint);
                intentPrint.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                break;


        }
    }
}
