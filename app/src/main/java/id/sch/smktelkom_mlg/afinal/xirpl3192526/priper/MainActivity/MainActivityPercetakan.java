package id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.MainActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

import com.google.firebase.auth.FirebaseAuth;

import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.About.TentangKami;
import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.InputDataPercetakan.ProfilePercetakan;
import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.LoginActivity;
import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.R;
import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.RecyclerRiwayatPercetakan.RiwayatPercetakanRecycler;
import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.StatusPrintPercetakan.RecyclerPesananActivity;

/**
 * Created by Amar on 29/04/2018.
 */

public class MainActivityPercetakan extends AppCompatActivity implements View.OnClickListener {

    private android.support.v7.widget.Toolbar toolbar;
    private FirebaseAuth auth;
    private FirebaseAuth.AuthStateListener authStateListener;
    private ImageButton Profil, History, Pesanan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_percetakan);

        Profil = findViewById(R.id.btn_profil);
        History = findViewById(R.id.btn_history);
        Pesanan = findViewById(R.id.btn_pesanan);

        Profil.setOnClickListener(this);
        History.setOnClickListener(this);
        Pesanan.setOnClickListener(this);

        auth = FirebaseAuth.getInstance();

        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() == null) {
                    Intent intentLogin = new Intent(MainActivityPercetakan.this, LoginActivity.class);
                    startActivity(intentLogin);
                    finish();
                }
            }
        };

        toolbar = findViewById(R.id.tb_main);
        setSupportActionBar(toolbar);
        toolbar.inflateMenu(R.menu.main_menu);
    }

    @Override
    public void onStart() {
        super.onStart();
        // Supaya Validasi User Sudah Login Atau Belum Berjalan
        auth.addAuthStateListener(authStateListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.about:
                startActivity(new Intent(MainActivityPercetakan.this, TentangKami.class));
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_profil:
                Intent ProfilIntent = new Intent(MainActivityPercetakan.this, ProfilePercetakan.class);
                startActivity(ProfilIntent);
                break;

            case R.id.btn_history:
                Intent RiwayatIntent = new Intent(MainActivityPercetakan.this, RiwayatPercetakanRecycler.class);
                startActivity(RiwayatIntent);
                break;

            case R.id.btn_pesanan:
                Intent PesananIntent = new Intent(MainActivityPercetakan.this, RecyclerPesananActivity.class);
                startActivity(PesananIntent);
                break;
        }
    }
}
