package id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.RecyclerPesananUser;

public class StatusPesanan {

    private String tanggal, status, id_transaksi, nama_percetakan, jam;


    public StatusPesanan(String tanggal, String status, String id_transaksi, String nama_percetakan, String jam) {
        this.tanggal = tanggal;
        this.status = status;
        this.id_transaksi = id_transaksi;
        this.nama_percetakan = nama_percetakan;
        this.jam = jam;
    }

    public StatusPesanan() {

    }

    public String gettanggal() {
        return tanggal;
    }

    public void settanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId_transaksi() {
        return id_transaksi;
    }

    public void setId_transaksi(String id_transaksi) {
        this.id_transaksi = id_transaksi;
    }

    public String getNama_percetakan() {
        return nama_percetakan;
    }

    public void setNama_percetakan(String nama_percetakan) {
        this.nama_percetakan = nama_percetakan;
    }

    public String getJam() {
        return jam;
    }

    public void setJam(String jam) {
        this.jam = jam;
    }
}
