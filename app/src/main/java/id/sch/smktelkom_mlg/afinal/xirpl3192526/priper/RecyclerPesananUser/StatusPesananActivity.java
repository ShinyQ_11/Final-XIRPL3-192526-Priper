package id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.RecyclerPesananUser;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.shashank.sony.fancytoastlib.FancyToast;

import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.CekKoneksi;
import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.R;
import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.StatusPrint.OrderAccepted;
import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.StatusPrint.OrderFinished;
import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.StatusPrint.OrderPending;
import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.StatusPrint.OrderRejected;

public class StatusPesananActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;
    private android.support.v7.widget.Toolbar toolbar;
    private RecyclerView recyclerStatus;
    private FirebaseAuth auth;
    private FirebaseUser user;
    private DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statususer);

        if (CekKoneksi.SatusInternet(StatusPesananActivity.this)) {
        } else {
            FancyToast.makeText(StatusPesananActivity.this, " Koneksi Internet Tidak Ada ! ", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
        }

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Tunggu Sebentar ...");
        progressDialog.show();

        toolbar = findViewById(R.id.tb_main);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        user = FirebaseAuth.getInstance().getCurrentUser();
        final String Uid = user.getUid();

        recyclerStatus = findViewById(R.id.rv_status);
        recyclerStatus.setLayoutManager(new LinearLayoutManager(this));

        auth = FirebaseAuth.getInstance();
        reference = FirebaseDatabase.getInstance().getReference().child("Costumer").child(Uid).child("Transaksi");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        user = FirebaseAuth.getInstance().getCurrentUser();
        final String Uid = user.getUid();
        auth = FirebaseAuth.getInstance();
        reference = FirebaseDatabase.getInstance().getReference().child("Costumer").child(Uid).child("Transaksi");

        final Query query = FirebaseDatabase.getInstance()
                .getReference()
                .child("Costumer")
                .child(Uid)
                .child("Transaksi");

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.getValue() == null) {
                    FancyToast.makeText(StatusPesananActivity.this, " Anda Belum Membuat Pesanan ", FancyToast.LENGTH_LONG, FancyToast.WARNING, false).show();
                }

                FirebaseRecyclerOptions<StatusPesanan> options =
                        new FirebaseRecyclerOptions.Builder<StatusPesanan>()
                                .setQuery(query, StatusPesanan.class)
                                .build();

                FirebaseRecyclerAdapter<StatusPesanan, StatusPesananViewHolder> recyclerAdapter = new FirebaseRecyclerAdapter<StatusPesanan, StatusPesananViewHolder>(options) {

                    @Override
                    protected void onBindViewHolder(@NonNull StatusPesananViewHolder holder, int position, @NonNull StatusPesanan model) {
                        final String key_transaksi = getRef(position).getKey();
                        holder.setNamaPercetakan(model.getNama_percetakan());
                        holder.setKodePesanan(model.getId_transaksi());
                        holder.setStatus(model.getStatus());
                        holder.setTanggal(model.gettanggal());
                        holder.setJam(model.getJam());

                        holder.Detail.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                user = FirebaseAuth.getInstance().getCurrentUser();
                                final String Uid = user.getUid();
                                reference = FirebaseDatabase.getInstance().getReference().child("Costumer").child(Uid).child("Transaksi").child(key_transaksi);

                                reference.addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {

                                        String status = dataSnapshot.child("status").getValue(String.class);

                                        if (status == null) {
                                            finish();
                                        } else if (status.equals("Selesai") || status == "Selesai") {

                                            Intent intentDetail = new Intent(StatusPesananActivity.this, OrderFinished.class);
                                            intentDetail.putExtra("KeyTransaksi", key_transaksi);
                                            startActivity(intentDetail);
                                            finish();

                                        } else if (status.equals("Pending") || status == "Pending") {
                                            Intent intentDetail = new Intent(StatusPesananActivity.this, OrderPending.class);
                                            intentDetail.putExtra("KeyTransaksi", key_transaksi);
                                            startActivity(intentDetail);
                                            finish();

                                        } else if (status.equals("Diterima") || status == "Diterima") {
                                            Intent intentDetail = new Intent(StatusPesananActivity.this, OrderAccepted.class);
                                            intentDetail.putExtra("KeyTransaksi", key_transaksi);
                                            startActivity(intentDetail);

                                        } else if (status.equals("Ditolak") || status == "Ditolak") {
                                            Intent intentDetail = new Intent(StatusPesananActivity.this, OrderRejected.class);
                                            intentDetail.putExtra("KeyTransaksi", key_transaksi);
                                            startActivity(intentDetail);

                                        } else {
                                            finish();
                                        }

                                    }


                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });

                            }
                        });
                    }

                    @NonNull
                    @Override
                    public StatusPesananViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                        View view = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.cardview_statususeer, parent, false);
                        return new StatusPesananViewHolder(view);
                    }
                };
                recyclerAdapter.startListening();
                recyclerStatus.setAdapter(recyclerAdapter);
                progressDialog.dismiss();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressDialog.dismiss();
            }
        });

    }

    public class StatusPesananViewHolder extends RecyclerView.ViewHolder {
        View mView;
        TextView Time, KodePesanan, Status, NamaPercetakan, Tanggal;
        Button Detail;

        private StatusPesananViewHolder(View itemView) {
            super(itemView);


            mView = itemView;
            Time = itemView.findViewById(R.id.tv_time);
            Tanggal = itemView.findViewById(R.id.tv_jam);
            KodePesanan = itemView.findViewById(R.id.tv_idPesanan);
            Status = itemView.findViewById(R.id.tv_status);
            NamaPercetakan = itemView.findViewById(R.id.tv_Percetakan);
            Detail = itemView.findViewById(R.id.btn_selengkapnya);

        }

        public void setTanggal(String tanggal) {
            Time.setText(tanggal);
        }

        public void setKodePesanan(String kodePesanan) {
            KodePesanan.setText(kodePesanan);
        }

        public void setStatus(String status) {
            Status.setText(status);
        }

        public void setNamaPercetakan(String namaPercetakan) {
            NamaPercetakan.setText(namaPercetakan);
        }

        public void setJam(String jam) {
            Tanggal.setText(jam);
        }

    }


}
