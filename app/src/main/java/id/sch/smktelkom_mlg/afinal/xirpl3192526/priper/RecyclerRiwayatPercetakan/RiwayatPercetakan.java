package id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.RecyclerRiwayatPercetakan;

public class RiwayatPercetakan {
    private String nama;
    private String id_transaksi;
    private String jam;
    private String status;
    private String tanggal;
    private String nama_percetakan;


    public RiwayatPercetakan(String nama, String id_transaksi, String jam, String status, String tanggal, String nama_percetakan) {
        this.nama = nama;
        this.id_transaksi = id_transaksi;
        this.jam = jam;
        this.status = status;
        this.tanggal = tanggal;
        this.nama_percetakan = nama_percetakan;
    }

    public RiwayatPercetakan() {

    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getId_transaksi() {
        return id_transaksi;
    }

    public void setId_transaksi(String id_transaksi) {
        this.id_transaksi = id_transaksi;
    }

    public String getJam() {
        return jam;
    }

    public void setJam(String jam) {
        this.jam = jam;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getNama_percetakan() {
        return nama_percetakan;
    }

    public void setNama_percetakan(String nama_percetakan) {
        this.nama_percetakan = nama_percetakan;
    }
}
