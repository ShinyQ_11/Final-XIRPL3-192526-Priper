package id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.RecyclerRiwayatPercetakan;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.shashank.sony.fancytoastlib.FancyToast;

import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.CekKoneksi;
import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.R;

public class RiwayatPercetakanRecycler extends AppCompatActivity {

    private ProgressDialog progressDialog;
    private android.support.v7.widget.Toolbar toolbar;
    private RecyclerView recyclerStatus;
    private FirebaseAuth auth;
    private FirebaseUser user;
    private DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_riwayat_percetakan);

        if (CekKoneksi.SatusInternet(RiwayatPercetakanRecycler.this)) {
        } else {
            FancyToast.makeText(RiwayatPercetakanRecycler.this, " Koneksi Internet Tidak Ada ! ", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
        }

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Tunggu Sebentar ...");
        progressDialog.show();

        toolbar = findViewById(R.id.tb_main);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        user = FirebaseAuth.getInstance().getCurrentUser();
        final String Uid = user.getUid();

        recyclerStatus = findViewById(R.id.rv_status);
        recyclerStatus.setLayoutManager(new LinearLayoutManager(this));

        auth = FirebaseAuth.getInstance();
        reference = FirebaseDatabase.getInstance().getReference().child("Costumer").child(Uid).child("Transaksi");

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();
        user = FirebaseAuth.getInstance().getCurrentUser();
        final String Uid = user.getUid();
        auth = FirebaseAuth.getInstance();
        reference = FirebaseDatabase.getInstance().getReference().child("Riwayat Percetakan")
                .child(Uid);

        final Query query = FirebaseDatabase
                .getInstance()
                .getReference()
                .child("Riwayat Percetakan")
                .child(Uid);

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null) {
                    FancyToast.makeText(RiwayatPercetakanRecycler.this, " Belum Ada Riwayat ", FancyToast.LENGTH_LONG, FancyToast.WARNING, false).show();
                }
                FirebaseRecyclerOptions<RiwayatPercetakan> options =
                        new FirebaseRecyclerOptions.Builder<RiwayatPercetakan>()
                                .setQuery(query, RiwayatPercetakan.class)
                                .build();

                FirebaseRecyclerAdapter<RiwayatPercetakan, RiwayatPercetakanRecycler.RiwayatPercetakanViewHolder> recyclerAdapter = new FirebaseRecyclerAdapter<RiwayatPercetakan, RiwayatPercetakanViewHolder>(options) {
                    @Override
                    protected void onBindViewHolder(@NonNull RiwayatPercetakanViewHolder holder, int position, @NonNull RiwayatPercetakan model) {
                        holder.setNama(model.getNama());
                        holder.setID(model.getId_transaksi());
                        holder.setTanggal(model.getTanggal());
                        holder.setJam(model.getJam());
                        holder.setStatus(model.getStatus());
                    }

                    @NonNull
                    @Override
                    public RiwayatPercetakanViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                        View view = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.cardview_riwayat_percetakan, parent, false);
                        return new RiwayatPercetakanRecycler.RiwayatPercetakanViewHolder(view);
                    }
                };

                recyclerAdapter.startListening();
                recyclerStatus.setAdapter(recyclerAdapter);
                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });

    }

    public class RiwayatPercetakanViewHolder extends RecyclerView.ViewHolder {
        View mView;
        TextView Nama, ID, Tanggal, Jam, Status;


        private RiwayatPercetakanViewHolder(View itemView) {
            super(itemView);

            mView = itemView;
            Nama = itemView.findViewById(R.id.tv_Percetakan);
            ID = itemView.findViewById(R.id.tv_idPesanan);
            Tanggal = itemView.findViewById(R.id.tv_tanggal);
            Jam = itemView.findViewById(R.id.tv_jam);
            Status = itemView.findViewById(R.id.tv_status);
        }

        public void setNama(String nama) {
            Nama.setText(nama);
        }

        public void setID(String id_transaksi) {
            ID.setText(id_transaksi);
        }

        public void setTanggal(String tanggal) {
            Tanggal.setText(tanggal);
        }

        public void setJam(String jam) {
            Jam.setText(jam);
        }

        public void setStatus(String status) {
            Status.setText(status);
        }

    }

}