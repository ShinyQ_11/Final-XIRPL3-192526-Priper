package id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.RecyclerTempatPercetakan;

public class DataPercetakan {

    private String nama;
    private String foto;
    private String alamat;
    private String jam;

    public DataPercetakan(String nama, String foto, String alamat, String jam) {
        this.nama = nama;
        this.foto = foto;
        this.alamat = alamat;
    }

    public DataPercetakan() {

    }


    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getJam() {
        return jam;
    }

    public void setJam(String jam) {
        this.jam = jam;
    }
}
