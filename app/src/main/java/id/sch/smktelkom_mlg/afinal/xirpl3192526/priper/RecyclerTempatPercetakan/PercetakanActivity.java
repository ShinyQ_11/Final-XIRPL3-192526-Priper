package id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.RecyclerTempatPercetakan;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.shashank.sony.fancytoastlib.FancyToast;

import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.CekKoneksi;
import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.InputDataUser.PrintFile;
import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.R;

public class PercetakanActivity extends AppCompatActivity {
    private android.support.v7.widget.Toolbar toolbar;
    private FirebaseAuth auth;
    private FirebaseUser user;
    private DatabaseReference reference;
    private ProgressDialog progressDialog;
    private RecyclerView recyclerStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_percetakan);

        if (CekKoneksi.SatusInternet(PercetakanActivity.this)) {
        } else {
            FancyToast.makeText(PercetakanActivity.this, " Koneksi Internet Tidak Ada ! ", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
        }

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Tunggu Sebentar ...");
        progressDialog.show();

        toolbar = findViewById(R.id.tb_main);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        user = FirebaseAuth.getInstance().getCurrentUser();
        final String Uid = user.getUid();

        recyclerStatus = findViewById(R.id.rv_status);
        recyclerStatus.setLayoutManager(new LinearLayoutManager(this));

        auth = FirebaseAuth.getInstance();
        reference = FirebaseDatabase.getInstance().getReference().child("Percetakan");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        user = FirebaseAuth.getInstance().getCurrentUser();
        auth = FirebaseAuth.getInstance();
        reference = FirebaseDatabase.getInstance().getReference().child("Percetakan");

        final Query query = FirebaseDatabase
                .getInstance()
                .getReference()
                .child("Data Percetakan");


        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                FirebaseRecyclerOptions<DataPercetakan> options =
                        new FirebaseRecyclerOptions.Builder<DataPercetakan>()
                                .setQuery(query, DataPercetakan.class)
                                .build();

                FirebaseRecyclerAdapter<DataPercetakan, PercetakanViewHolder> recyclerAdapter = new FirebaseRecyclerAdapter<DataPercetakan, PercetakanViewHolder>(options) {
                    @Override
                    protected void onBindViewHolder(@NonNull PercetakanViewHolder holder, int position, @NonNull DataPercetakan model) {
                        final String id_percetakan = getRef(position).getKey();
                        holder.setNama(model.getNama());
                        holder.setAlamat(model.getAlamat());
                        holder.setFoto(getApplicationContext(), model.getFoto());

                        holder.Pilih.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                reference = FirebaseDatabase.getInstance().getReference().child("Data Percetakan").child(id_percetakan);
                                reference.addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        String id_percetakan = dataSnapshot.getKey();
                                        String Nama = dataSnapshot.child("nama").getValue(String.class);
                                        Intent intentDetail = new Intent(PercetakanActivity.this, PrintFile.class);
                                        intentDetail.putExtra("id_percetakan", id_percetakan);
                                        intentDetail.putExtra("NamaPercetakan", Nama);
                                        startActivity(intentDetail);
                                        finish();
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });
                            }
                        });


                    }

                    @NonNull
                    @Override
                    public PercetakanViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                        View view = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.cardview_percetakan, parent, false);
                        return new PercetakanActivity.PercetakanViewHolder(view);
                    }
                };
                recyclerAdapter.startListening();
                recyclerStatus.setAdapter(recyclerAdapter);
                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressDialog.dismiss();
            }
        });
    }

    public static class PercetakanViewHolder extends RecyclerView.ViewHolder {
        View mView;
        TextView Nama, Alamat;
        ImageView Foto;
        Button Pilih, Profil;


        private PercetakanViewHolder(View itemView) {
            super(itemView);

            mView = itemView;
            Nama = itemView.findViewById(R.id.tv_nama);
            Alamat = itemView.findViewById(R.id.alamat);
            Foto = itemView.findViewById(R.id.foto);
            Pilih = itemView.findViewById(R.id.pilih);
            Profil = itemView.findViewById(R.id.profil);
        }

        public void setNama(String nama) {
            Nama.setText(nama);
        }

        public void setAlamat(String alamat) {
            Alamat.setText(alamat);
        }

        public void setFoto(Context ctx, String foto) {
            if (foto == null) {
                Foto.setImageResource(R.drawable.print);
            } else {
                Glide.with(ctx)
                        .load(foto)
                        .into(Foto);
            }
        }

    }

}

