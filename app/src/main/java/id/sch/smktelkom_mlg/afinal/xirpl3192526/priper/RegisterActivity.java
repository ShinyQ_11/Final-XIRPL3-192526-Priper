package id.sch.smktelkom_mlg.afinal.xirpl3192526.priper;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.shashank.sony.fancytoastlib.FancyToast;

import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.MainActivity.MainActivity;
import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.MainActivity.MainActivityPercetakan;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText Nama, Telp, Email, Username, Password, Alamat;
    private Button Registrasi;
    private TextView Login;
    private DatabaseReference reference, ref;
    private FirebaseAuth mAuth;
    private Spinner SpinnerJenisPengguna;
    private ArrayAdapter<CharSequence> adapterJenisPengguna;
    private ProgressDialog progressDialog;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mAuth = FirebaseAuth.getInstance();
        reference = FirebaseDatabase.getInstance().getReference().child("Costumer");

        progressDialog = new ProgressDialog(this);
        Nama = findViewById(R.id.edt_nama);
        Telp = findViewById(R.id.edt_telp);
        Email = findViewById(R.id.edt_email);
        Username = findViewById(R.id.edt_username);
        Password = findViewById(R.id.edt_password);
        Alamat = findViewById(R.id.edt_alamat);

        Login = findViewById(R.id.tv_login);
        Login.setOnClickListener(this);

        Registrasi = findViewById(R.id.btn_daftar);
        Registrasi.setOnClickListener(this);

        SpinnerJenisPengguna = findViewById(R.id.spinnerJenisPengguna);
        adapterJenisPengguna = ArrayAdapter.createFromResource(this, R.array.Jenis_Pengguna, android.R.layout.simple_spinner_item);
        adapterJenisPengguna.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SpinnerJenisPengguna.setAdapter(adapterJenisPengguna);


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_daftar:
                daftar();
                finish();
                break;

            case R.id.tv_login:
                Intent IntentRegister = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(IntentRegister);
                IntentRegister.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
        }

    }

    public void daftar() {
        if (Nama.getText().toString().length() == 0) {
            FancyToast.makeText(RegisterActivity.this, " Nama Harus Diisi ! ", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
        } else if (Telp.getText().toString().length() == 0) {
            FancyToast.makeText(RegisterActivity.this, " Nomor Telepon Harus Diisi ! ", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
        } else if (Email.getText().toString().length() == 0) {
            FancyToast.makeText(RegisterActivity.this, " Email Harus Diisi ! ", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
        } else if (Alamat.getText().toString().length() == 0) {
            FancyToast.makeText(RegisterActivity.this, " Alamat Harus Diisi ! ", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
        } else if (Username.getText().toString().length() == 0) {
            FancyToast.makeText(RegisterActivity.this, " Username Harus Diisi ! ", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
        } else if (Password.getText().toString().length() == 0) {
            FancyToast.makeText(RegisterActivity.this, " Password Harus Diisi ! ", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
        } else if (Password.getText().toString().length() < 6) {
            FancyToast.makeText(RegisterActivity.this, " Minimal Password Harus 6 Digit ", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
        } else {
            progressDialog.setMessage("Tunggu Sebentar ...");
            progressDialog.show();
            final String name = Nama.getText().toString().trim();
            final String telp = Telp.getText().toString().trim();
            final String username = Username.getText().toString().trim();
            final String email = Email.getText().toString().trim();
            final String password = Password.getText().toString().trim();
            final String alamat = Alamat.getText().toString().trim();
            final String jenisPengguna = SpinnerJenisPengguna.getSelectedItem().toString().trim();

            mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {

                    if (task.isSuccessful()) {
                        String user_id = mAuth.getCurrentUser().getUid();
                        DatabaseReference userdb = reference.child(user_id).child("Data User");
                        userdb.child("nama").setValue(name);
                        userdb.child("telp").setValue(telp);
                        userdb.child("username").setValue(username);
                        userdb.child("email").setValue(email);
                        userdb.child("password").setValue(password);
                        userdb.child("alamat").setValue(alamat);
                        userdb.child("jenis_pengguna").setValue(jenisPengguna);
                        userdb.child("foto").setValue("");

                        FancyToast.makeText(RegisterActivity.this, "Registrasi Sukses", FancyToast.LENGTH_LONG, FancyToast.SUCCESS, false).show();

                        if (jenisPengguna.equals("Pengguna") || jenisPengguna == "Pengguna") {
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                public void run() {

                                    Intent IntentRegister = new Intent(RegisterActivity.this, MainActivity.class);
                                    startActivity(IntentRegister);
                                    finish();
                                }
                            }, 1000);
                        } else {
                            reference = FirebaseDatabase.getInstance().getReference().child("Percetakan").child(user_id);
                            DatabaseReference percetakandb = reference.child("Data Percetakan");
                            final String name = Nama.getText().toString().trim();
                            final String telp = Telp.getText().toString().trim();
                            final String username = Username.getText().toString().trim();
                            final String email = Email.getText().toString().trim();
                            final String password = Password.getText().toString().trim();
                            final String jenisPengguna = SpinnerJenisPengguna.getSelectedItem().toString().trim();

                            percetakandb.child("nama").setValue(name);
                            percetakandb.child("telp").setValue(telp);
                            percetakandb.child("username").setValue(username);
                            percetakandb.child("email").setValue(email);
                            percetakandb.child("password").setValue(password);
                            percetakandb.child("alamat").setValue(alamat);
                            percetakandb.child("jenis_pengguna").setValue(jenisPengguna);

                            ref = FirebaseDatabase.getInstance().getReference().child("Data Percetakan");
                            DatabaseReference percetakan = ref.child(user_id);
                            percetakan.child("nama").setValue(name);
                            percetakan.child("telp").setValue(telp);
                            percetakan.child("username").setValue(username);
                            percetakan.child("email").setValue(email);
                            percetakan.child("password").setValue(password);
                            percetakan.child("alamat").setValue(alamat);
                            percetakan.child("jenis_pengguna").setValue(jenisPengguna);

                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                public void run() {
                                    progressDialog.dismiss();
                                    Intent IntentRegister = new Intent(RegisterActivity.this, MainActivityPercetakan.class);
                                    startActivity(IntentRegister);
                                    IntentRegister.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    finish();
                                }
                            }, 1000);
                        }
                    } else {
                        progressDialog.dismiss();
                        FancyToast.makeText(RegisterActivity.this, "Email Tidak Valid", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
                    }
                }
            });
        }
    }
}
