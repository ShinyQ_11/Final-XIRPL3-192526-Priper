package id.sch.smktelkom_mlg.afinal.xirpl3192526.priper;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.shashank.sony.fancytoastlib.FancyToast;

import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.MainActivity.MainActivity;
import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.MainActivity.MainActivityPercetakan;

/**
 * Created by Kurniadi A.W on 3/29/2018.
 */

public class SplashScreen extends AppCompatActivity {
    private TextView tvSplash;
    private FirebaseAuth auth;
    private FirebaseAuth.AuthStateListener authStateListener;
    private DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        tvSplash = findViewById(R.id.tvSplash);

        if (CekKoneksi.SatusInternet(SplashScreen.this)) {
        } else {
            FancyToast.makeText(SplashScreen.this, " Koneksi Internet Tidak Ada ! ", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
        }
        auth = FirebaseAuth.getInstance();

        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() == null) {
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intentLogin = new Intent(SplashScreen.this, LoginActivity.class);
                            startActivity(intentLogin);
                            finish();
                        }
                    }, 1000L); //2000 L = 2 detik
                } else {
                    final String user_uid = auth.getCurrentUser().getUid();

                    reference = FirebaseDatabase.getInstance().getReference().child("Costumer").child(user_uid).child("Data User");
                    reference.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            String JenisPengguna = dataSnapshot.child("jenis_pengguna").getValue(String.class);

                            if (JenisPengguna == null) {
                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                                        finish();
                                    }
                                }, 1000L); //2000 L = 2 detik
                            } else if (JenisPengguna.equals("Pengguna") || JenisPengguna == "Pengguna") {
                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                                        finish();

                                    }
                                }, 1000L); //2000 L = 2 detik
                            } else {
                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        startActivity(new Intent(getApplicationContext(), MainActivityPercetakan.class));
                                        finish();
                                    }
                                }, 1000L); //2000 L = 2 detik
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                                    finish();
                                }
                            }, 1000L); //2000 L = 2 detik
                        }
                    });

                }
            }
        };

    }

    @Override
    public void onStart() {
        super.onStart();
        // Supaya Validasi User Sudah Login Atau Belum Berjalan
        auth.addAuthStateListener(authStateListener);
    }

}

