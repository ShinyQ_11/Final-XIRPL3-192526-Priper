package id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.StatusPrint;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.R;

public class OrderAccepted extends AppCompatActivity implements View.OnClickListener {

    private FirebaseAuth auth;
    private TextView KodePesanan, Jumlah, Ukuran, Orientasi, Tinta, Catatan;
    private DatabaseReference reference;
    private android.support.v7.widget.Toolbar toolbar;
    private FirebaseUser user;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_accepted);

        reference = FirebaseDatabase.getInstance().getReference().child("Costumer");

        KodePesanan = findViewById(R.id.tv_uidPesanan);
        Jumlah = findViewById(R.id.tv_jumlah);
        Ukuran = findViewById(R.id.tv_ukuran);
        Orientasi = findViewById(R.id.tv_orientasi);
        Tinta = findViewById(R.id.tv_tinta);
        Catatan = findViewById(R.id.catatan);

        auth = FirebaseAuth.getInstance();
        toolbar = findViewById(R.id.tb_main);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getPesanan();
    }

    public void getPesanan() {
        user = FirebaseAuth.getInstance().getCurrentUser();
        final String Uid = user.getUid();
        final String KeyTransaksi = getIntent().getStringExtra("KeyTransaksi");
        reference = FirebaseDatabase.getInstance().getReference().child("Costumer");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String jumlah = dataSnapshot.child(Uid).child("Transaksi").child(KeyTransaksi).child("jumlah").getValue(String.class);
                String ukuran = dataSnapshot.child(Uid).child("Transaksi").child(KeyTransaksi).child("jenis_kertas").getValue(String.class);
                String orientasi = dataSnapshot.child(Uid).child("Transaksi").child(KeyTransaksi).child("jenis_orientasi").getValue(String.class);
                String tinta = dataSnapshot.child(Uid).child("Transaksi").child(KeyTransaksi).child("jenis_tinta").getValue(String.class);
                String catatan = dataSnapshot.child(Uid).child("Transaksi").child(KeyTransaksi).child("catatan").getValue(String.class);

                KodePesanan.setText(KeyTransaksi);
                Jumlah.setText(jumlah);
                Ukuran.setText(ukuran);
                Orientasi.setText(orientasi);
                Tinta.setText(tinta);
                if (catatan.length() == 0) {
                    Catatan.setText("-");
                } else {
                    Catatan.setText(catatan);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

    }
}
