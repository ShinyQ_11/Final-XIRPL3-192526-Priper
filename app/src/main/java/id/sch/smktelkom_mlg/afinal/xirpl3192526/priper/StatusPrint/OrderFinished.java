package id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.StatusPrint;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.shashank.sony.fancytoastlib.FancyToast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.MainActivity.MainActivity;
import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.R;

public class OrderFinished extends AppCompatActivity implements View.OnClickListener {

    private Button Konfirmasi;
    private FirebaseAuth auth;
    private DatabaseReference reference;
    private android.support.v7.widget.Toolbar toolbar;
    private FirebaseUser user;
    private Calendar calendar;
    private SimpleDateFormat simpleDateFormat;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_finished);

        reference = FirebaseDatabase.getInstance().getReference().child("Costumer");
        Konfirmasi = findViewById(R.id.btn_konfirmasi);

        auth = FirebaseAuth.getInstance();
        toolbar = findViewById(R.id.tb_main);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getPesanan();
    }

    public void getPesanan() {
        user = FirebaseAuth.getInstance().getCurrentUser();
        final String Uid = user.getUid();
        final String KeyTransaksi = getIntent().getStringExtra("KeyTransaksi");
        reference = FirebaseDatabase.getInstance().getReference().child("Costumer").child(Uid).child("Transaksi").child(KeyTransaksi);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final String nama = dataSnapshot.child("nama").getValue(String.class);
                final String namaPercetakan = dataSnapshot.child("nama_percetakan").getValue(String.class);

                Konfirmasi.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        FirebaseDatabase database = FirebaseDatabase.getInstance();
                        final String key = database.getReference("Riwayat User").push().getKey();

                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("Riwayat User");
                        final DatabaseReference riwayat = ref.child(Uid).child(key);
                        riwayat.addValueEventListener(new ValueEventListener() {
                            @SuppressLint("SimpleDateFormat")
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                FancyToast.makeText(OrderFinished.this, " Pesanan Selesai", FancyToast.LENGTH_LONG, FancyToast.SUCCESS, false).show();
                                calendar = Calendar.getInstance();
                                final String tanggal = DateFormat.getDateInstance(DateFormat.FULL).format(calendar.getTime());

                                calendar = Calendar.getInstance();
                                simpleDateFormat = new SimpleDateFormat("HH:mm");
                                final String jam = simpleDateFormat.format(calendar.getTime());

                                riwayat.child("id_transaksi").setValue(KeyTransaksi);
                                riwayat.child("id_user").setValue(Uid);
                                riwayat.child("status").setValue("Selesai");
                                riwayat.child("nama").setValue(nama);
                                riwayat.child("nama_percetakan").setValue(namaPercetakan);
                                riwayat.child("jam").setValue(jam);
                                riwayat.child("tanggal").setValue(tanggal);

                                Intent intentOk = new Intent(OrderFinished.this, MainActivity.class);
                                startActivity(intentOk);

                                reference.removeValue();
                                finish();
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });


                    }


                });
            }


            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

    }
}
