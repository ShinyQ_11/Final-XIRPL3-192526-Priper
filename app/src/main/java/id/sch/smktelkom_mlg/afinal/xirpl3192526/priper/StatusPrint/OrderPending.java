package id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.StatusPrint;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.MainActivity.MainActivity;
import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.R;
import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.RecyclerPesananUser.StatusPesananActivity;

public class OrderPending extends AppCompatActivity implements View.OnClickListener {

    private Calendar calendar;
    private SimpleDateFormat simpleDateFormat;
    private FirebaseAuth auth;
    private Button Ok, Batal;
    private TextView KodePesanan, Jumlah, Ukuran, Orientasi, Tinta, Catatan;
    private DatabaseReference reference, idPercetakan;
    private android.support.v7.widget.Toolbar toolbar;
    private FirebaseUser user;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_pending);

        reference = FirebaseDatabase.getInstance().getReference().child("Costumer");

        KodePesanan = findViewById(R.id.tv_uidPesanan);
        Jumlah = findViewById(R.id.tv_jumlah);
        Ukuran = findViewById(R.id.tv_ukuran);
        Orientasi = findViewById(R.id.tv_orientasi);
        Tinta = findViewById(R.id.tv_tinta);
        Catatan = findViewById(R.id.catatan);
        Ok = findViewById(R.id.btn_ok);
        Ok.setOnClickListener(this);
        Batal = findViewById(R.id.btn_batal);
        Batal.setOnClickListener(this);

        auth = FirebaseAuth.getInstance();
        toolbar = findViewById(R.id.tb_main);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getPesanan();
    }

    public void getPesanan() {
        user = FirebaseAuth.getInstance().getCurrentUser();
        final String Uid = user.getUid();
        final String KeyTransaksi = getIntent().getStringExtra("KeyTransaksi");
        reference = FirebaseDatabase.getInstance().getReference().child("Costumer");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final String id_percetakan = dataSnapshot.child(Uid).child("Transaksi").child(KeyTransaksi).child("id_percetakan").getValue(String.class);
                final String jumlah = dataSnapshot.child(Uid).child("Transaksi").child(KeyTransaksi).child("jumlah").getValue(String.class);
                final String ukuran = dataSnapshot.child(Uid).child("Transaksi").child(KeyTransaksi).child("jenis_kertas").getValue(String.class);
                final String orientasi = dataSnapshot.child(Uid).child("Transaksi").child(KeyTransaksi).child("jenis_orientasi").getValue(String.class);
                final String tinta = dataSnapshot.child(Uid).child("Transaksi").child(KeyTransaksi).child("jenis_tinta").getValue(String.class);
                final String catatan = dataSnapshot.child(Uid).child("Transaksi").child(KeyTransaksi).child("catatan").getValue(String.class);
                final String nama = dataSnapshot.child(Uid).child("Transaksi").child(KeyTransaksi).child("nama").getValue(String.class);
                final String namaPercetakan = dataSnapshot.child(Uid).child("Transaksi").child(KeyTransaksi).child("nama_percetakan").getValue(String.class);

                Batal.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(
                                OrderPending.this);
                        alert.setTitle("Batalkan Pesanan");
                        alert.setMessage("Anda Yakin Ingin Membatalkan ?");
                        alert.setPositiveButton("Ya", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {


                                FirebaseDatabase database = FirebaseDatabase.getInstance();
                                final String key = database.getReference("Riwayat User").push().getKey();

                                DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("Riwayat User");
                                final DatabaseReference riwayat = ref.child(Uid).child(key);
                                riwayat.addValueEventListener(new ValueEventListener() {
                                    @SuppressLint("SimpleDateFormat")
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {

                                        calendar = Calendar.getInstance();
                                        final String tanggal = DateFormat.getDateInstance(DateFormat.FULL).format(calendar.getTime());

                                        calendar = Calendar.getInstance();
                                        simpleDateFormat = new SimpleDateFormat("HH:mm");
                                        final String jam = simpleDateFormat.format(calendar.getTime());

                                        riwayat.child("id_transaksi").setValue(KeyTransaksi);
                                        riwayat.child("id_percetakan").setValue(id_percetakan);
                                        riwayat.child("status").setValue("Pesanan Dibatalkan");
                                        riwayat.child("jam").setValue(jam);
                                        riwayat.child("tanggal").setValue(tanggal);
                                        riwayat.child("nama").setValue(nama);
                                        riwayat.child("nama_percetakan").setValue(namaPercetakan);


                                        Intent intentOk = new Intent(OrderPending.this, MainActivity.class);
                                        startActivity(intentOk);

                                        idPercetakan = FirebaseDatabase.getInstance().getReference().child("Percetakan").child(id_percetakan).child("Transaksi").child(KeyTransaksi);
                                        idPercetakan.removeValue();

                                        DatabaseReference idUser = FirebaseDatabase.getInstance().getReference().child("Costumer").child(Uid).child("Transaksi").child(KeyTransaksi);
                                        idUser.removeValue();

                                        finish();
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });

                            }
                        });
                        alert.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        alert.show();
                    }
                });

                KodePesanan.setText(KeyTransaksi);
                Jumlah.setText(jumlah);
                Ukuran.setText(ukuran);
                Orientasi.setText(orientasi);
                Tinta.setText(tinta);
                if (catatan == null) {
                    Catatan.setText("-");
                } else if (catatan.length() == 0) {
                    Catatan.setText("-");
                } else {
                    Catatan.setText(catatan);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_ok:
                Intent intentOk = new Intent(OrderPending.this, StatusPesananActivity.class);
                startActivity(intentOk);
                finish();
                break;

        }
    }
}