package id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.StatusPrint;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.shashank.sony.fancytoastlib.FancyToast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.MainActivity.MainActivity;
import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.R;

public class OrderRejected extends AppCompatActivity implements View.OnClickListener {

    private Button Hapus;
    private FirebaseAuth auth;
    private TextView KodePesanan, Jumlah, Ukuran, Orientasi, Tinta, Catatan;
    private DatabaseReference reference, idPercetakan;
    private android.support.v7.widget.Toolbar toolbar;
    private FirebaseUser user;
    private Calendar calendar;
    private SimpleDateFormat simpleDateFormat;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_rejected);

        reference = FirebaseDatabase.getInstance().getReference().child("Costumer");

        Hapus = findViewById(R.id.btn_hapus);
        KodePesanan = findViewById(R.id.tv_uidPesanan);
        Jumlah = findViewById(R.id.tv_jumlah);
        Ukuran = findViewById(R.id.tv_ukuran);
        Orientasi = findViewById(R.id.tv_orientasi);
        Tinta = findViewById(R.id.tv_tinta);
        Catatan = findViewById(R.id.catatan);

        auth = FirebaseAuth.getInstance();
        toolbar = findViewById(R.id.tb_main);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getPesanan();
    }

    public void getPesanan() {
        user = FirebaseAuth.getInstance().getCurrentUser();
        final String Uid = user.getUid();
        final String KeyTransaksi = getIntent().getStringExtra("KeyTransaksi");
        reference = FirebaseDatabase.getInstance().getReference().child("Costumer").child(Uid).child("Transaksi").child(KeyTransaksi);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String jumlah = dataSnapshot.child("jumlah").getValue(String.class);
                String ukuran = dataSnapshot.child("jenis_kertas").getValue(String.class);
                String orientasi = dataSnapshot.child("jenis_orientasi").getValue(String.class);
                String tinta = dataSnapshot.child("jenis_tinta").getValue(String.class);
                String catatan = dataSnapshot.child("catatan").getValue(String.class);
                final String nama = dataSnapshot.child("nama").getValue(String.class);
                final String namaPercetakan = dataSnapshot.child("nama_percetakan").getValue(String.class);
                final String id_percetakan = dataSnapshot.child("id_percetakan").getValue(String.class);

                KodePesanan.setText(KeyTransaksi);
                Jumlah.setText(jumlah);
                Ukuran.setText(ukuran);
                Orientasi.setText(orientasi);
                Tinta.setText(tinta);
                if (catatan == null || catatan.equals("")) {
                    Catatan.setText("-");
                } else {
                    Catatan.setText(catatan);
                }

                Hapus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        FirebaseDatabase database = FirebaseDatabase.getInstance();
                        final String key = database.getReference("Riwayat User").push().getKey();

                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("Riwayat User");
                        final DatabaseReference riwayat = ref.child(Uid).child(KeyTransaksi).child(key);
                        riwayat.addValueEventListener(new ValueEventListener() {
                            @SuppressLint("SimpleDateFormat")
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {

                                calendar = Calendar.getInstance();
                                final String tanggal = DateFormat.getDateInstance(DateFormat.FULL).format(calendar.getTime());

                                calendar = Calendar.getInstance();
                                simpleDateFormat = new SimpleDateFormat("HH:mm");
                                final String jam = simpleDateFormat.format(calendar.getTime());
                                riwayat.child("id_transaksi").setValue(KeyTransaksi);
                                riwayat.child("id_percetakan").setValue(KeyTransaksi);
                                riwayat.child("status").setValue("Pesanan Ditolak");
                                riwayat.child("jam").setValue(jam);
                                riwayat.child("tanggal").setValue(tanggal);
                                riwayat.child("nama").setValue(nama);
                                riwayat.child("nama_percetakan").setValue(namaPercetakan);

                                DatabaseReference idUser = FirebaseDatabase.getInstance().getReference().child("Costumer").child(Uid).child("Transaksi").child(KeyTransaksi);
                                idUser.child("status").setValue("Pesanan Ditolak");


                                Intent intentOk = new Intent(OrderRejected.this, MainActivity.class);
                                FancyToast.makeText(OrderRejected.this, " Pesanan Dihapus", FancyToast.LENGTH_LONG, FancyToast.SUCCESS, false).show();
                                startActivity(intentOk);

                                idUser.removeValue();

                                finish();
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });


                    }


                });
            }



            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

    }
}
