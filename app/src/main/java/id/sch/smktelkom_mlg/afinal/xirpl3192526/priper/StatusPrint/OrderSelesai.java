package id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.StatusPrint;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.shashank.sony.fancytoastlib.FancyToast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.MainActivity.MainActivityPercetakan;
import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.R;

public class OrderSelesai extends AppCompatActivity {
    private FirebaseAuth auth;
    private Button Konfirmasi;
    private Calendar calendar;
    private SimpleDateFormat simpleDateFormat;
    private TextView KodePesanan, Jumlah, Ukuran, Orientasi, Tinta, Catatan, File, Nama, Email, Telp;
    private DatabaseReference reference, idPercetakan;
    private android.support.v7.widget.Toolbar toolbar;
    private FirebaseUser user;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_konfirmasi);

        reference = FirebaseDatabase.getInstance().getReference().child("Percetakan");

        Nama = findViewById(R.id.tv_pemesan);
        Email = findViewById(R.id.tv_userEmail);
        Telp = findViewById(R.id.tv_telp);
        KodePesanan = findViewById(R.id.tv_uidPesanan);
        Jumlah = findViewById(R.id.tv_jumlah);
        Ukuran = findViewById(R.id.tv_ukuran);
        Orientasi = findViewById(R.id.tv_orientasi);
        Tinta = findViewById(R.id.tv_tinta);
        Catatan = findViewById(R.id.catatan);
        File = findViewById(R.id.file);
        Konfirmasi = findViewById(R.id.btn_konfirmasi);


        auth = FirebaseAuth.getInstance();
        toolbar = findViewById(R.id.tb_main);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getPesanan();
    }

    public void getPesanan() {
        user = FirebaseAuth.getInstance().getCurrentUser();
        final String Uid = user.getUid();
        final String id_transaksi = getIntent().getStringExtra("id_transaksi");
        reference = FirebaseDatabase.getInstance().getReference().child("Percetakan");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final String id_user = dataSnapshot.child(Uid).child("Transaksi").child(id_transaksi).child("id_user").getValue(String.class);
                final String jumlah = dataSnapshot.child(Uid).child("Transaksi").child(id_transaksi).child("jumlah").getValue(String.class);
                final String ukuran = dataSnapshot.child(Uid).child("Transaksi").child(id_transaksi).child("jenis_kertas").getValue(String.class);
                final String orientasi = dataSnapshot.child(Uid).child("Transaksi").child(id_transaksi).child("jenis_orientasi").getValue(String.class);
                final String tinta = dataSnapshot.child(Uid).child("Transaksi").child(id_transaksi).child("jenis_tinta").getValue(String.class);
                final String catatan = dataSnapshot.child(Uid).child("Transaksi").child(id_transaksi).child("catatan").getValue(String.class);
                final String nama = dataSnapshot.child(Uid).child("Transaksi").child(id_transaksi).child("nama").getValue(String.class);
                final String namaPercetakan = dataSnapshot.child(Uid).child("Transaksi").child(id_transaksi).child("nama_percetakan").getValue(String.class);
                final String file = dataSnapshot.child(Uid).child("Transaksi").child(id_transaksi).child("file").getValue(String.class);
                KodePesanan.setText(id_transaksi);
                Jumlah.setText(jumlah);
                Ukuran.setText(ukuran);
                Orientasi.setText(orientasi);
                Tinta.setText(tinta);
                if (catatan == null) {
                    Catatan.setText("-");
                } else if (catatan.length() == 0) {
                    Catatan.setText("-");
                } else {
                    Catatan.setText(catatan);
                }
                File.setText(file);

                Konfirmasi.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(
                                OrderSelesai.this);
                        alert.setTitle("Konfirmasi Pesanan");
                        alert.setMessage("Pesanan Sudah Selesai ?");
                        alert.setPositiveButton("Ya", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                FirebaseDatabase database = FirebaseDatabase.getInstance();
                                final String key = database.getReference("Riwayat Percetakan").push().getKey();

                                DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("Riwayat Percetakan");
                                final DatabaseReference riwayat = ref.child(Uid).child(key);
                                riwayat.addValueEventListener(new ValueEventListener() {
                                    @SuppressLint("SimpleDateFormat")
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {

                                        calendar = Calendar.getInstance();
                                        final String tanggal = DateFormat.getDateInstance(DateFormat.FULL).format(calendar.getTime());

                                        calendar = Calendar.getInstance();
                                        simpleDateFormat = new SimpleDateFormat("HH:mm");
                                        final String jam = simpleDateFormat.format(calendar.getTime());

                                        riwayat.child("id_transaksi").setValue(id_transaksi);
                                        riwayat.child("id_percetakan").setValue(Uid);
                                        riwayat.child("status").setValue("Selesai");
                                        riwayat.child("nama").setValue(nama);
                                        riwayat.child("nama_percetakan").setValue(namaPercetakan);
                                        riwayat.child("jam").setValue(jam);
                                        riwayat.child("tanggal").setValue(tanggal);

                                        DatabaseReference idUser = FirebaseDatabase.getInstance().getReference().child("Costumer").child(id_user).child("Transaksi").child(id_transaksi);
                                        idUser.child("status").setValue("Selesai");


                                        Intent intentOk = new Intent(OrderSelesai.this, MainActivityPercetakan.class);
                                        FancyToast.makeText(OrderSelesai.this, " Pesanan Selesai", FancyToast.LENGTH_LONG, FancyToast.SUCCESS, false).show();
                                        startActivity(intentOk);
                                        idPercetakan = FirebaseDatabase.getInstance().getReference().child("Percetakan").child(Uid).child("Transaksi").child(id_transaksi);
                                        idPercetakan.removeValue();
                                        finish();
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });

                            }
                        });
                        alert.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        alert.show();
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
