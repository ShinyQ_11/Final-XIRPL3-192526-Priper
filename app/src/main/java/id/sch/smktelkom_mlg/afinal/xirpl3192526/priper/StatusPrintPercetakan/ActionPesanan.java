package id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.StatusPrintPercetakan;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.shashank.sony.fancytoastlib.FancyToast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.MainActivity.MainActivityPercetakan;
import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.R;

public class ActionPesanan extends AppCompatActivity implements View.OnClickListener {
    private String TERIMA = "Diterima";
    private String TOLAK = "Ditolak";
    private android.support.v7.widget.Toolbar toolbar;
    private FirebaseAuth auth;
    private FirebaseUser user;
    private DatabaseReference reference, ref, stat;
    private ProgressDialog progressDialog;
    private Calendar calendar;
    private SimpleDateFormat simpleDateFormat;
    private Button Terima, Tolak;
    private TextView KodePesanan, Jumlah, Ukuran, Orientasi, Tinta, Catatan, File, Nama, Email, Telp;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_action);

        Nama = findViewById(R.id.tv_pemesan);
        Email = findViewById(R.id.tv_userEmail);
        Telp = findViewById(R.id.tv_telp);
        KodePesanan = findViewById(R.id.tv_uidPesanan);
        Jumlah = findViewById(R.id.tv_jumlah);
        Ukuran = findViewById(R.id.tv_ukuran);
        Orientasi = findViewById(R.id.tv_orientasi);
        Tinta = findViewById(R.id.tv_tinta);
        Catatan = findViewById(R.id.catatan);
        File = findViewById(R.id.file);

        Terima = findViewById(R.id.button_terima);
        Tolak = findViewById(R.id.button_tolak);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Tunggu Sebentar ...");
        progressDialog.show();

        user = FirebaseAuth.getInstance().getCurrentUser();
        final String Uid = user.getUid();
        reference = FirebaseDatabase.getInstance().getReference().child("Percetakan").child(Uid).child("Transaksi");
        auth = FirebaseAuth.getInstance();

        toolbar = findViewById(R.id.tb_main);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        user = FirebaseAuth.getInstance().getCurrentUser();
        final String id_transaksi = getIntent().getStringExtra("id_transaksi");
        reference = FirebaseDatabase.getInstance().getReference().child("Percetakan").child(Uid).child("Transaksi").child(id_transaksi);
        reference.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                String id_costumer = dataSnapshot.child("id_user").getValue(String.class);
                String kodepesanan = dataSnapshot.child("id_transaksi").getValue(String.class);
                String jumlah = dataSnapshot.child("jumlah").getValue(String.class);
                String ukuran = dataSnapshot.child("jenis_kertas").getValue(String.class);
                String orientasi = dataSnapshot.child("jenis_orientasi").getValue(String.class);
                String tinta = dataSnapshot.child("jenis_tinta").getValue(String.class);
                String catatan = dataSnapshot.child("catatan").getValue(String.class);
                String file = dataSnapshot.child("file").getValue(String.class);
                final String nama = dataSnapshot.child("nama").getValue(String.class);

                KodePesanan.setText(kodepesanan);
                Jumlah.setText(jumlah);
                Ukuran.setText(ukuran);
                Orientasi.setText(orientasi);
                Tinta.setText(tinta);
                Catatan.setText(catatan);

                stat = FirebaseDatabase.getInstance().getReference().child("Costumer").child(id_costumer).child("Transaksi").child(id_transaksi);
                Terima.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        FancyToast.makeText(ActionPesanan.this, " Pesanan Diterima ", FancyToast.LENGTH_LONG, FancyToast.SUCCESS, false).show();
                        reference.child("status").setValue(TERIMA);
                        stat.child("status").setValue(TERIMA);
                        Intent intentPesanan = new Intent(ActionPesanan.this, RecyclerPesananActivity.class);
                        startActivity(intentPesanan);

                    }
                });

                Tolak.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        FancyToast.makeText(ActionPesanan.this, " Pesanan Dihapus", FancyToast.LENGTH_LONG, FancyToast.SUCCESS, false).show();
                        FirebaseDatabase database = FirebaseDatabase.getInstance();
                        final String key = database.getReference("Riwayat Percetakan").push().getKey();
                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("Riwayat Percetakan");
                        final DatabaseReference riwayat = ref.child(Uid).child(key);
                        riwayat.addValueEventListener(new ValueEventListener() {
                            @SuppressLint("SimpleDateFormat")
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {

                                calendar = Calendar.getInstance();
                                final String tanggal = DateFormat.getDateInstance(DateFormat.FULL).format(calendar.getTime());

                                calendar = Calendar.getInstance();
                                simpleDateFormat = new SimpleDateFormat("HH:mm");
                                final String jam = simpleDateFormat.format(calendar.getTime());

                                riwayat.child("id_transaksi").setValue(id_transaksi);
                                riwayat.child("id_percetakan").setValue(Uid);
                                riwayat.child("status").setValue("Pesanan Ditolak");
                                riwayat.child("nama").setValue(nama);
                                riwayat.child("jam").setValue(jam);
                                riwayat.child("tanggal").setValue(tanggal);

                                reference.child("status").setValue(TOLAK);
                                stat.child("status").setValue(TOLAK);
                                Intent intentPesanan = new Intent(ActionPesanan.this, MainActivityPercetakan.class);
                                startActivity(intentPesanan);
                                reference.removeValue();
                                finish();
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    }
                });

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    File.setText(Html.fromHtml(file, Html.FROM_HTML_MODE_COMPACT));
                } else {
                    File.setText(Html.fromHtml(file));
                }

                ref = FirebaseDatabase.getInstance().getReference().child("Costumer").child(id_costumer).child("Data User");
                ref.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String nama = dataSnapshot.child("nama").getValue(String.class);
                        String email = dataSnapshot.child("email").getValue(String.class);
                        String telp = dataSnapshot.child("telp").getValue(String.class);

                        Nama.setText(nama);
                        Email.setText(email);
                        Telp.setText(telp);
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    @Override
    public void onClick(View v) {

    }
}

