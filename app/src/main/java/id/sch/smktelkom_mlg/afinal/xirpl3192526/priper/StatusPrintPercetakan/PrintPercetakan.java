package id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.StatusPrintPercetakan;

public class PrintPercetakan {
    private String nama;
    private String id_transaksi;
    private String tanggal;
    private String jam;

    public PrintPercetakan(String nama, String id_transaksi, String tanggal, String jam) {
        this.nama = nama;
        this.id_transaksi = id_transaksi;
        this.tanggal = tanggal;
        this.jam = jam;
    }

    public PrintPercetakan() {

    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getId_transaksi() {
        return id_transaksi;
    }

    public void setId_transaksi(String id_transaksi) {
        this.id_transaksi = id_transaksi;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getJam() {
        return jam;
    }

    public void setJam(String jam) {
        this.jam = jam;
    }
}
