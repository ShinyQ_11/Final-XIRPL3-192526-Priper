package id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.StatusPrintPercetakan;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.shashank.sony.fancytoastlib.FancyToast;

import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.R;
import id.sch.smktelkom_mlg.afinal.xirpl3192526.priper.StatusPrint.OrderSelesai;

public class RecyclerPesananActivity extends AppCompatActivity {
    private android.support.v7.widget.Toolbar toolbar;
    private FirebaseAuth auth;
    private FirebaseUser user;
    private DatabaseReference reference;
    private ProgressDialog progressDialog;
    private RecyclerView recyclerStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actionpesanan);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Tunggu Sebentar ...");
        progressDialog.show();

        toolbar = findViewById(R.id.tb_main);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        user = FirebaseAuth.getInstance().getCurrentUser();
        final String Uid = user.getUid();

        recyclerStatus = findViewById(R.id.rv_status);
        recyclerStatus.setLayoutManager(new LinearLayoutManager(this));

        auth = FirebaseAuth.getInstance();
        reference = FirebaseDatabase.getInstance().getReference().child("Percetakan");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        user = FirebaseAuth.getInstance().getCurrentUser();
        final String Uid = user.getUid();
        auth = FirebaseAuth.getInstance();
        reference = FirebaseDatabase.getInstance().getReference().child("Percetakan").child("Percetakan")
                .child(Uid)
                .child("Transaksi");

        final Query query = FirebaseDatabase
                .getInstance()
                .getReference()
                .child("Percetakan")
                .child(Uid)
                .child("Transaksi");

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.getValue() == null) {
                    FancyToast.makeText(RecyclerPesananActivity.this, " Belum Ada Pesanan Masuk ", FancyToast.LENGTH_LONG, FancyToast.WARNING, false).show();
                }
                FirebaseRecyclerOptions<PrintPercetakan> options =
                        new FirebaseRecyclerOptions.Builder<PrintPercetakan>()
                                .setQuery(query, PrintPercetakan.class)
                                .build();

                FirebaseRecyclerAdapter<PrintPercetakan, RecyclerPesananActivity.PercetakanPrintViewHolder> recyclerAdapter = new FirebaseRecyclerAdapter<PrintPercetakan, PercetakanPrintViewHolder>(options) {
                    @Override
                    protected void onBindViewHolder(@NonNull PercetakanPrintViewHolder holder, int position, @NonNull PrintPercetakan model) {
                        final String id_transaksi = getRef(position).getKey();
                        holder.setNama(model.getNama());
                        holder.setID(model.getId_transaksi());
                        holder.setTanggal(model.getTanggal());
                        holder.setJam(model.getJam());
                        holder.Detail.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                user = FirebaseAuth.getInstance().getCurrentUser();
                                final String Uid = user.getUid();
                                reference = FirebaseDatabase.getInstance().getReference().child("Percetakan").child(Uid).child("Transaksi").child(id_transaksi);
                                reference.addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        String status = dataSnapshot.child("status").getValue(String.class);

                                        if (status == null) {
                                            finish();
                                        } else if (status.equals("Diterima") || status == "Diterima") {
                                            Intent intentKonfirmasi = new Intent(RecyclerPesananActivity.this, OrderSelesai.class);
                                            intentKonfirmasi.putExtra("id_transaksi", id_transaksi);
                                            startActivity(intentKonfirmasi);
                                            finish();
                                        } else {
                                            Intent intentDetail = new Intent(RecyclerPesananActivity.this, ActionPesanan.class);
                                            intentDetail.putExtra("id_transaksi", id_transaksi);
                                            startActivity(intentDetail);
                                            finish();
                                        }

                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });

                            }
                        });

                    }

                    @NonNull
                    @Override
                    public PercetakanPrintViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                        View view = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.cardview_pesananuser, parent, false);
                        return new RecyclerPesananActivity.PercetakanPrintViewHolder(view);
                    }
                };

                recyclerAdapter.startListening();
                recyclerStatus.setAdapter(recyclerAdapter);
                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    public class PercetakanPrintViewHolder extends RecyclerView.ViewHolder {
        View mView;
        TextView Nama, ID, Time, Jam, Status;
        Button Detail;


        private PercetakanPrintViewHolder(View itemView) {
            super(itemView);

            mView = itemView;
            Nama = itemView.findViewById(R.id.tv_namaUser);
            ID = itemView.findViewById(R.id.tv_idPesanan);
            Time = itemView.findViewById(R.id.tv_time);
            Jam = itemView.findViewById(R.id.tv_jam);
            Status = itemView.findViewById(R.id.tv_status);
            Detail = itemView.findViewById(R.id.btn_selengkapnya);
        }

        public void setNama(String nama) {
            Nama.setText(nama);
        }

        public void setID(String id_transaksi) {
            ID.setText(id_transaksi);
        }

        public void setTanggal(String tanggal) {
            Time.setText(tanggal);
        }

        public void setJam(String jam) {
            Jam.setText(jam);
        }

    }

}


